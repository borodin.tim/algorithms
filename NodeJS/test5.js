const EventEmitter = require('events');
const emitter = new EventEmitter();

//Register a listener
emitter.on('messageLogged', (eventArg) => {
    console.log('Listener called', eventArg);
});

// Raisse an event
emitter.emit('messageLogged', {id: 1, url: 'http://'});

// Raise: logging (data: message)

