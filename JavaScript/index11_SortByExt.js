// You are given a list of files. 
// You need to sort this list by the file extension. 
// The files with the same extension should be sorted by name.

// Some possible cases:
//     Filename cannot be an empty string;
//     Files without the extension should go before the files with one;
//     Filename ".config" has an empty extension and a name ".config";
//     Filename "config." has an empty extension and a name "config.";
//     Filename "table.imp.xls" has an extension "xls" and a name "table.imp";
//     Filename ".imp.xls" has an extension "xls" and a name ".imp".

// Input: A list of filenames.

// Output: A list of filenames. 

// Simple sort files by extension:
//let arr = Array.from(files.sort((a, b) => a.substring(a.lastIndexOf('.')) > b.substring(b.lastIndexOf('.'))));

function customSort(){
    return function(a, b) {
        if(a.lastIndexOf('.') == -1 && b.lastIndexOf('.') == -1){
            // go here if there's no dots
            if(a > b){
                return 1;
            }else {
                return -1;
            }
        } else if(a.lastIndexOf('.') == -1){
            return -1;
        } else if(b.lastIndexOf('.') == -1){
            return 1;
        } else
        
        if( a.substring(a.lastIndexOf('.')) == b.substring(b.lastIndexOf('.')) ){
            // go here if extensions are equal
            if(a.substring(0, a.lastIndexOf('.')).length == 0 && b.substring(0, b.lastIndexOf('.')).length == 0 ){
                if( a.substring(a.lastIndexOf('.')) > b.substring(b.lastIndexOf('.')) ){
                    return 1;
                } else {
                    return -1;
                }
            } else if( a.substring(0, a.lastIndexOf('.')).length == 0 ){
                return -1;
            } else if(b.substring(0, b.lastIndexOf('.')).length == 0){
                return 1;
            } else if( a.substring(0, a.lastIndexOf('.')) > b.substring(0, b.lastIndexOf('.')) ){
                return 1;
            }else {
                return -1;
            }
        } else 
        // go here if extensions are different
        if(a.substring(0, a.lastIndexOf('.')).length == 0 && b.substring(0, b.lastIndexOf('.')).length == 0 ){
            // go here if names are empty
            if( a.substring(a.lastIndexOf('.')) > b.substring(b.lastIndexOf('.')) ){
                return 1;
            } else {
                return -1;
            }
        } else if( a.substring(0, a.lastIndexOf('.')).length == 0 ){
            return -1;
        } else if(b.substring(0, b.lastIndexOf('.')).length == 0){
            return 1;
        } else if( a.substring(a.lastIndexOf('.')) > b.substring(b.lastIndexOf('.')) ){
            return 1;
        } else {
            return -1;
        }
        return 0;
    };
}

function sortByExt(files) {
    //console.log(files[0].substring(files[0].lastIndexOf('.')));
    files.sort(customSort());
    console.log(files);
    return files;
}

sortByExt(['1.aa', '.bat', '.aa', '.cat']);
sortByExt(['3.bat', '1.aa', '2.bat']);
sortByExt(['ccc', 'ddd', '1.cad', '1.bat', '1.aa', 'asd']);
sortByExt(['aaa', '1.cad', '1.bat', '1.aa', '2.bat', 'ccc']);
sortByExt(['1.cad', '1.bat', '1.aa', '.bat']);
sortByExt(['1.cad', '1.bat', '.aa', '.bat']);
sortByExt(['1.cad', '1.', '1.aa']);
sortByExt(['1.cad', '1.bat', '1.aa', '1.aa.doc']);
sortByExt(['1.cad', '1.bat', '1.aa', '.aa.doc']);


// assert.deepEqual(sortByExt(['1.cad', '1.bat', '1.aa']), ['1.aa', '1.bat', '1.cad']);
// assert.deepEqual(sortByExt(['1.cad', '1.bat', '1.aa', '2.bat']), ['1.aa', '1.bat', '2.bat', '1.cad']);
// assert.deepEqual(sortByExt(['1.cad', '1.bat', '1.aa', '.bat']), ['.bat', '1.aa', '1.bat', '1.cad']);
// assert.deepEqual(sortByExt(['1.cad', '1.bat', '.aa', '.bat']), ['.aa', '.bat', '1.bat', '1.cad']);
// assert.deepEqual(sortByExt(['1.cad', '1.', '1.aa']), ['1.', '1.aa', '1.cad']);
// assert.deepEqual(sortByExt(['1.cad', '1.bat', '1.aa', '1.aa.doc']), ['1.aa', '1.bat', '1.cad', '1.aa.doc']);
// assert.deepEqual(sortByExt(['1.cad', '1.bat', '1.aa', '.aa.doc']), ['1.aa', '1.bat', '1.cad', '.aa.doc']);