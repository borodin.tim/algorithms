// You have to split a given array into two arrays.
// If it has an odd amount of elements, then the first array should have more elements. If it has no elements, then two empty arrays should be returned.
// [1,2,3,4,5]
// [1,2,3][4,5]
// Input: List.
// Output: List of two lists. 

console.log('round=' + Math.round(3 / 2));

let values = [1, 2, 3, 4, 5, 6];
let resu = [[], []];
let j=0;
for (let i = 0; i < values.length; i++) {
    const element = values[i];
    if(i < Math.round(values.length / 2)){
        resu[0][i] = element;
        
    } else {
        resu[1][j] = element;
        j++;
    }
}

for (let i = 0; i < resu.length; i++) {
    //for (let j = 0; j < resu[i].length; j++) {
       // const element = resu[i][j];
        //console.log(element);        
    //}
    //console.log('/n');        
    console.log(resu[i]);
}
console.log('resu=' + resu);