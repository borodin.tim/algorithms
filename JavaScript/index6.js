// You are given a string and two markers (the initial and final). You have to find a substring enclosed between these two markers. But there are a few important conditions:
// The initial and final markers are always different.
// If there is no initial marker, then the first character should be considered the beginning of a string.
// If there is no final marker, then the last character should be considered the ending of a string.
// If the initial and final markers are missing then simply return the whole string.
// If the final marker comes before the initial marker, then return an empty string.
// Input: Three arguments. All of them are strings. The second and third arguments are the initial and final markers.
// Output: A string.
// Precondition: can't be more than one final marker and can't be more than one initial. Marker can't be an empty string 

// let text = '<head><title>My new site</title></head>';
// let begin = '<title>';
// let end = '</title>';

// let text = 'What is >apple<';
// let begin = '>';
// let end = '<';

let text = 'Never send a human to do a machine\'s job.';
let begin = 'Never';
let end = 'do';

let res = '';
let s=0, e=0;
let s1=0;

s = text.indexOf(begin);
e = text.indexOf(end);

// if(s < 0){
//     s = 0;
// }
if(e < 0){
    e = text.length;
}
if(s<e){
    if(s >= 0){
        s1 = s + begin.length;
    } else if( s == -1) {
        s1 = 0;
    }
    res = text.substring(s1,e);
}
console.log('res=' + res);
