// You are given a text, which contains different english letters and punctuation symbols. You should find the most frequent letter
// in the text. The letter returned must be in lower case.
// While checking for the most wanted letter, casing does not matter, so for the purpose of your search, "A" == "a". 
// Make sure you do not count punctuation symbols, digits and whitespaces, only letters.

// If you have two or more letters with the same frequency, then return the letter which comes first in the latin alphabet. 
// For example -- "one" contains "o", "n", "e" only once for each, thus we choose "e".

// Input: A text for analysis as a string.
// Output: The most frequent letter in lower case as a string.
// Example:
// mostWanted('Hello World!') == 'l'
// mostWanted('How do you do?') == 'o'
// mostWanted('One') == 'e'
// mostWanted('Oops!') == 'o'
// mostWanted('AAaooo!!!!') == 'a'

// How it is used: For most decryption tasks you need to know the frequency of occurrence for various letters in a section of text. 
// For example: we can easily crack a simple addition or substitution cipher if we know the frequency in which letters appear. 
// This is interesting stuff for language experts!
// Precondition:  A text contains only ASCII symbols.  0 < len(text) ≤ 105 

function mostWanted(text) {
    let array = text.toLowerCase().match(/[a-z]/g);
    let arrayOccurences = [];
    for (let i = 0; i < array.length; i++) {
        //console.log('i=' + i);
        let idx = arrayOccurences.findIndex((item) => { return item.letter === array[i] });
        //console.log('idx=' + idx);
        if (idx >= 0) {
            //console.log('idx=' + idx + ' letter=' + array[i]);
            //console.log('arrayOccurences[2].letter=' + arrayOccurences[2].letter);
            arrayOccurences[idx].count++;
            //console.log('arrayOccurences[idx].letter=' + arrayOccurences[idx].letter);
        } else {
            console.log('Pushing:' + array[i]);
            arrayOccurences.push({
                letter: array[i],
                count: 1
            });
            //console.log('arrayOccurences.length:=' + arrayOccurences.length);
        }
    }
    // console.log('arrayOccurences.length=' + arrayOccurences.length); 
    // console.log(arrayOccurences.forEach((obj) => { console.log(`letter: $(obj.letter), count: $(obj.count)`); }));
    console.log('arrayOccurences:=');
    arrayOccurences = arrayOccurences.sort((a, b) => { return (a.letter > b.letter) ? 1 : -1 });
    arrayOccurences.forEach(elem => console.log('letter: ' + elem.letter + '; count: ' + elem.count));

    // TODO find the first elem with highest count:
    // TODO 1. find highest count
    let maxCount = Math.max.apply( Math, arrayOccurences.map((elem) => {return elem.count}));
    console.log( 'maxCount=' + maxCount );


    let maxCountIndex = arrayOccurences.findIndex((elem) => {return elem.count === maxCount});
    console.log( 'maxCountIndex=' + maxCountIndex );

    return arrayOccurences[maxCountIndex].letter.toString();
}

// console.log(mostWanted('one')); // e
// console.log('=============================');
console.log(mostWanted('One'));
