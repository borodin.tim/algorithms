// You have an array. Each value from that array can be either a string or an integer. 
// Your task here is to return two values. The first one is a concatenation of all strings from the given array. 
// The second one is a sum of all integers from the given array.

// Input: A list of strings and ints

// Output: An array

// Precondition: both given ints should be between -1000 and 1000 

"use strict";

function sumByTypes(values) {
    let resu = ['', 0];

    for (let i = 0; i < values.length; i++) {
        let value = values[i];
        if(typeof value === 'number'){
            value = parseInt(value);
            //console.log('typeof value=' + typeof value);
            console.log('typeof resu[1]=' + typeof resu[1]);
            resu[1] += value;
        } else if(typeof value === 'string'){
            //let val1 = (resu[0] === '') ? 0 : parseInt(resu[0]);
            //let val2 = (value === '' || isNaN(value)) ? 0 : parseInt(value);
            resu[0] = resu[0].concat(value);
        }
    }
    return resu;
}


console.log(sumByTypes([]));
console.log(sumByTypes([1, 2, 3]));
console.log(sumByTypes(['1', '2', 2, 3]));
//console.log(sumByTypes(['1', 2, 3, 'siz2e']));
//console.log(sumByTypes(['1', '2', '3']));
//console.log(sumByTypes(['size', 12, 'in', 45, 0]));



// Type Script code
// function sumByTypesTypeScript(values: Array<number|string>): [string, number] {
//     let resu: [string, number] = ['', 0];

//     for (let i = 0; i < values.length; i++) {
//         let value = values[i];
//         if(typeof value == 'number'){
//            (resu[1] as number) += (value  as number);
//         } else if(typeof value == 'string'){
//            (resu[0] as string) = (resu[0] as string).concat(value as string);
//         }
//     }
//     return resu;
// }