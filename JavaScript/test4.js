// const s = '$4.58';
const s = '5.69';



let rest = 0.0;
let dollars = 0;
let quarters = 0;
let dimes = 0;
let nickels = 0;
let pennies = 0;

if (s && s.length > 0 && s.charAt(0) === '$') {
    const num = parseFloat(s.substring(1));

    if (!isNaN(num)) {
        dollars = parseInt(num / 1);
        rest = parseFloat(num % 1);

        quarters = parseInt(rest / 0.25);
        rest = parseFloat(rest % 0.25);

        dimes = parseInt(rest / 0.1);
        rest = parseFloat(rest % 0.1).toFixed(2);

        nickels = parseInt(rest / 0.05);
        rest = parseFloat(rest % 0.05).toFixed(2);

        pennies = parseInt(rest / 0.01);
        rest = parseFloat(rest % 0.01).toFixed(2);
    }
}

console.log(dollars + ' ' + quarters + ' ' + dimes + ' ' + nickels + ' ' + pennies);

