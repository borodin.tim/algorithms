// Maybe it's a cipher? Maybe, but we don’t know for sure.
// Maybe you can call it "homomorphism"? i wish I know this word before.

// You need to check that the 2 given strings are isometric. This means that a character from one string can become a match for characters from another string.
// One character from one string can correspond only to one character from another string. Two or more characters of one string can correspond to one character of another string, but not vice versa.
// Input: Two arguments. Both strings.
// Output: Boolean.
// Precondition: both strings are the same size 

let str1 = 'aab';
let str2 = 'xzy';
//let str2 = 'xyz';
let element1, element2;
let res = true;
let arr = [];

for (let i = 0; i < str1.length; i++) {
    element1 = str1[i];
    element2 = str2[i];
    console.log('element1=' + element1);
    console.log('exp res=' + !arr.some(e => e.elem1 === element1));
    if ( !arr.some(e => e.elem1 === element1) ){
        arr[i] = {elem1:element1, elem2:element2};
        console.log(arr[i]);
    } else {
        let idx = arr.findIndex(e => e.elem1 === element1);
        console.log(idx);
        if(arr[idx].elem2 != element2){
            res = false;
            break;
        }
    }
}

console.log('res=' + res);

