// Check if a given string has all symbols in upper case. If the string is empty or doesn't have any letter in it - function should return False.
// Input: A string.
// Output: a boolean.
// Precondition: a-z, A-Z, 1-9 and spaces 

function isAllUpper(text) {
  let resu = false;
  let arra = [];
  //console.log("text.length="+text.length);
  if (text.length > 0 && containsLetters(text)) {
    resu = true;
    arra = text.split('');
    console.log('arra = \'' + arra + '\'');
    for (let i = 0; i < arra.length; i++) {
      const item = arra[i];
      if (item !== undefined && item !== item.toUpperCase()) {
        resu = false;
        break;
      }
    }
  }
  return resu;
}

function containsLetters(text) {
  return text.match(/[a-z]/i);
}

// console.log('Example:');
// console.log(isAllUpper('ALL UPPER'));
// console.log(isAllUpper2('all lower'));
// console.log(isAllUpper2('mixed UPPER and lower'));
// console.log(isAllUpper(''));

const foo = () => {
  // const str = 'Ask Any thing';
  // const searchStr = 'Ask thing';
  const str = 'Foo Bar Baz';
  const searchStr = 'foo baz';
  // const str = 'Workspace';
  // const searchStr = 'workspace';

  // const splitArr = searchStr.toLowerCase().split(' ');
  // // console.log('🚀 ~ file: index12_AllUpper.js ~ line 40 ~ foo ~ splitArr', splitArr);

  // const arrJoined = splitArr.join('|');
  // // console.log('🚀 ~ file: index12_AllUpper.js ~ line 43 ~ foo ~ arrJoined', arrJoined);

  // const regExp = new RegExp(arrJoined, 'g');
  // // const regExp = new RegExp('ask|thing');
  // console.log('🚀 ~ file: index12_AllUpper.js ~ line 46 ~ foo ~ regExp', regExp);

  // const res = [...str.toLowerCase().matchAll(regExp)];
  // console.log(res);

  // const res2 = regExp.test(str.toLowerCase());
  // console.log('res2:', res2);

  // const res3 = new RegExp(searchStr.toLowerCase().split(' ').join('|')).test(str.toLowerCase());
  // console.log('res3:', res3);
  // console.log('res3 regexp:', new RegExp(searchStr.toLowerCase().split(' ').join('|'), 'g').test(str.toLowerCase()));

  // const res3 = new RegExp(searchFilter.toLowerCase().split(' ').join('|'), 'g').test(workspace.title.toLowerCase());

  // console.log('includes: ', str.toLowerCase().includes(searchStr.toLowerCase()));

  // console.log('regex:', searchStr.toLowerCase().split(' ').join('|'));
  // const searchPattern = new RegExp(searchStr.toLowerCase().split(' ').join('|'));
  // console.log('test:', searchPattern.test(str.toLowerCase()));







  ///////////////////////////////////
  const rawWorkspaces = [
    {
      title: 'workspace 1',
      boardCount: 2
    },
    {
      title: 'team 33',
      boardCount: 14
    },
    {
      title: 'workspace 2',
      boardCount: 4
    },
    {
      title: 'asd',
      boardCount: 4
    },
    {
      title: 'vbn',
      boardCount: 5
    },
    {
      title: 'assass',
      boardCount: 15
    },
    {
      title: 'opopop',
      boardCount: 155
    },
  ];
  // const searchFilter = 'w 4 15';
  // const searchFilter = 'w 4 5';
  // const searchFilter = 'w 4';
  const searchFilter = 'workspace 4 15';

  const titlePattern = new RegExp(searchFilter.split(' ').join('|'), 'i')
  const extractedNumbersNotString = searchFilter.match(/(\d+)/g);
  console.log('🚀 ~ file: index12_AllUpper.js ~ line 114 ~ foo ~ extractedNumbersNotString', extractedNumbersNotString);
  // const extractedNumbers = searchFilter.match(/(\d+)/g).toString();
  // console.log('🚀 ~ file: index12_AllUpper.js ~ line 106 ~ foo ~ extractedNumbers', extractedNumbers);
  // const boardNumPattern = new RegExp(extractedNumbers.split(',').join('|'))
  // console.log('🚀 ', boardNumPattern);


  const userWorkspaces = rawWorkspaces.
    filter((workspace) => (workspace.boardCount > 0 || showEmptyWorkspaces) && (titlePattern.test(workspace.title) ||
      (extractedNumbersNotString && checkBoardCount(extractedNumbersNotString, workspace.boardCount))
    )).
    sort((a, b) => {
      if ((a.boardCount === 0 && b.boardCount === 0) || (a.boardCount !== 0 && b.boardCount !== 0)) {
        return a.title.localeCompare(b.title)
      }

      return b.boardCount - a.boardCount
    })


  console.log('userWorkspaces:', userWorkspaces);
};

const checkBoardCount = (numsArr, boardCount) => {
  for (let n of numsArr) {
    if (+n === boardCount) {
      return true;
    }
  }
  return false;
}

foo();

// workspace.boardCount.toString().includes(searchFilter)
// boardNumPattern.test(workspace.boardCount.toString())
// boardNumPattern.test(workspace.boardCount