// You are given a non-empty list of integers (X). For this task, you should return a list consisting of only the non-unique elements in this list. To do so you will need to remove all unique elements (elements which are contained in a given list only once). When solving this task, do not change the order of the list. Example: [1, 2, 3, 1, 3] 1 and 3 non-unique elements and result will be [1, 3, 1, 3].

// non-unique-elements

// Input: A list of integers.

// Output: An iterable of integers.

// How it is used: This mission will help you to understand how to manipulate arrays, something that is the basis for solving more complex tasks. The concept can be easily generalized for real world tasks. For example: if you need to clarify statistics by removing low frequency elements (noise).

// You can find out more about JavaScript arrays in one of our articles featuring lists, tuples, array.array and numpy.array.

// Precondition:
// 0 < len(data) < 1000

let data = [1,2,3,1,3];
let counter = 0;
let retArr = [];
for (let i = 0; i < data.length; i++) {
    counter = 0;
    for (let j = 0; j < data.length; j++) {
        if(data[i] === data [j]){
            counter++;
        }
    }
    if(counter > 1){
        retArr.push(data[i]);
    }
}
console.log('retArr=' + retArr);


///////////////////////////////////////////////
function nonUniqueElements(data) {
    const occurrences = data.reduce((accum, number) => {
        accum[number] = accum[number] ? accum[number] + 1 : 1 
        return accum;
    }, {})  
    return data.filter(n => occurrences[n] > 1);
}
const occurrences = data.reduce((accum, number) => {
    accum[number] = accum[number] ? accum[number] + 1 : 1 
    return accum;
}, {}) 
console.log('occurrences='+occurrences);
console.log('nonUniqueElements=' + nonUniqueElements([1,2,3,1,3]));


///////////////////////////////////////////////
//deleting elems from array
var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
for( var i = 0; i < arr.length; i++){
     if ( arr[i] === 5) { arr.splice(i, 1); 
    }
}//=> [1, 2, 3, 4, 6, 7, 8, 9, 0]

var arr = [1, 2, 3, 4, 5, 5, 6, 7, 8, 5, 9, 0];
for( var i = 0; i < arr.length; i++){ 
    if ( arr[i] === 5) { arr.splice(i, 1); 
        i--; 
    }
}
console.log('arr' + arr);
//=> [1, 2, 3, 4, 6, 7, 8, 9, 0]

function arrayRemove(arr, value) { 
    return arr.filter(function(ele){ return ele != value; });
}
let array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
var result = arrayRemove(array, 6);// result = [1, 2, 3, 4, 5, 7, 8, 9, 0]
console.log('arrayRemove = ' + result);