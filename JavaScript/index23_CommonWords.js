// Let's continue examining words. You are given two string with words separated by commas.
// Try to find what is common between these strings. The words are not repeated in 
// the same string.

// Your function should find all of the words that appear in both strings. 
// The result must be represented as a string of words separated by commas in alphabetic order.

// Input: Two arguments as strings.
// Output: The common words as a string.

// Example:
// commonWords('hello,world', 'hello,earth') == 'hello'
// commonWords('one,two,three', 'four,five,six') == ''
// commonWords('one,two,three', 'four,five,one,two,six,three') == 'one,three,two'

// How it is used: Here you can learn how to work with strings and sets. 
// This knowledge can be useful for linguistic analysis.

// Precondition:
// Each string contains no more than 10 words.
// All words separated by commas.
// All words consist of lowercase latin letters. 

function commonWords(line1, line2) {
    let arr1 = line1.split(',');
    let arr2 = line2.split(',');
    let arrResult = [];
    for (let i = 0; i < arr1.length; i++) {
        const element = arr1[i];
        if (arr2.includes(element)){
            arrResult.push(element);
        }
    }
    return arrResult.sort().join();
}

console.log(commonWords('hello,world', 'hello,earth')); // hello
console.log(commonWords('one,two,three', 'four,five,six')); // ''
console.log(commonWords('one,two,three','four,five,one,two,six,three')); // 'one,three,two'


