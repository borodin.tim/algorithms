// In this mission your task is to determine the popularity of certain words in the text.

// At the input of your function are given 2 arguments: the text and the array of words the popularity of which you need to determine.

// When solving this task pay attention to the following points:

//     The words should be sought in all registers. This means that if you need to find a word "one" then words like "one", "One", "oNe", "ONE" etc. will do.
//     The search words are always indicated in the lowercase.
//     If the word wasn’t found even once, it has to be returned in the dictionary with 0 (zero) value.

// Input: The text and the search words array.

// Output: The dictionary where the search words are the keys and values are the number of times when those words are occurring in a given text.

// Precondition:
// The input text will consists of English letters in uppercase and lowercase and whitespaces. 

function popularWords(text, words) {
    // your code here
    let map = {};
    let arr = text.split(/\s/);
    for (let i = 0; i < words.length; i++) {
       map[words[i]] = 0;    
    }
    for (let i = 0; i < arr.length; i++) {
        let word = arr[i].toLowerCase();
        if(words.includes(word)){
           map[word] = (map[word] + 1) || 1;
        }
    }
    console.log(map);
    return map;
}
console.log(popularWords(`
When I was One
I had just begun
When I was Two
I was nearly new`, ['i', 'was', 'three', 'near']));
console.log(['i', 'was', 'three', 'near'].includes('i').toLowerCase);

// Does the same as above
function popularWords(text: string, words: string[]) {
    return words.reduce((acc, word) => ({ ...acc, [word]: text.match(new RegExp(`\\b${word}\\b`, 'gi'))?.length || 0 }), {});
}
