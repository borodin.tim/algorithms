
// You prefer a good old 12-hour time format. But the modern world we live in would rather use the 24-hour format and you see it everywhere. Your task is to convert the time from the 24-h format into 12-h format by following the next rules:
// - the output format should be 'hh:mm a.m.' (for hours before midday) or 'hh:mm p.m.' (for hours after midday)
// - if hours is less than 10 - don't write a '0' before it. For example: '9:05 a.m.'


function timeConverter(dayTime) {
    let timeArr = dayTime.split(':');
    let label = 'a.m.';
    if(timeArr[0] >= 12){
        label = 'p.m.';
    }
    if(timeArr[0] > 12){
        timeArr[0] -= 12;
    }
    if(timeArr[0].toString().slice(0, 1) == 0){
        if(timeArr[0].toString().slice(1, 2) == 0){
            timeArr[0] = 12;
        } else {
            timeArr[0] = timeArr[0].slice(1);
        }
    }
    return timeArr[0] + ':' + timeArr[1] + ' ' + label;
}


console.log(timeConverter('12:30'));
console.log(timeConverter('09:00'));
console.log(timeConverter('21:00'));
console.log(timeConverter('00:00'));
console.log(timeConverter('24:00'));