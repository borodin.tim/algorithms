// In this mission you should check if all elements in the given Array are equal.
// Input: Array.
// Output: Bool.
// The idea for this mission was found on Python Tricks series by Dan Bader


//let elements = [1, 1, 1];
let elements = [1, 1,2,  1];

// simple solution
// for (let i = 0; i < elements.length-1; i++) {
//     if(elements[i] !== elements[i+1]){
//         resu = false;
//     }   
// }

// resu = elements.reduce((a, b) => {return a && b;});

let resu = [];
let arr2 = [];
resu = Array.from(elements.sort((a, b) => {return a > b;}));
arr2 = Array.from(elements.sort((a, b) => {return a < b;}));
console.log('resu=' + resu);
console.log('arr2=' + arr2);
console.log(JSON.stringify(resu) == JSON.stringify(arr2));
