const n = '2';
const text = ' abcde ';

// // console.log('n:', n)
// // console.log(text)
// // console.log(loops)

// for (let i = 0; i < loops; i++) {
//     const str = text.substring(i, i + n);
//     console.log(str);
// }

if (n && n.length > 0 && text && text.length > 0) {
    const num = parseInt(n);
    if (!isNaN(num)) {
        const loops = text.length - num + 1;
        for (let i = 0; i < loops; i++) {
            const str = text.substring(i, i + num);
            console.log(str);
        }
    }
}
