// return first word of a string
let text = '.Hasd\'s asd';
let wordStart = text.search(/[a-z]/i);
let substr = text.substring(wordStart);
// console.log('substr=' + substr);
let wordEnd = wordStart + substr.search(/[^a-z|'\'']/i);
// console.log('firstLetterIdx=' + wordStart);
// console.log('firstSpaceAfterLetterIdx=' + wordEnd);
// console.log('text.substr=\'' + text.substring(wordStart, wordEnd)+'\'');
///////////////////////////////////

///////////////////////////////////
// count digits in a string
let text2 = 'This 1999 is a 25 text';
let counter2 = 0;
let num;
let el;
for(el of text2){
    num = parseInt(el);
    if(!isNaN(num)){
        counter2++;
    }
}
// console.log('counter2=' + counter2);

//same:
let text3 = ' asd 1 asd 1987';
const digits = text3.match(/\d/g) || []
// console.log(digits.length);
///////////////////////////////////


///////////////////////////////////
// In a given string you should reverse every word, but the words should stay in their places.
// Input: A string. Output: A string.
// Precondition: The line consists only from alphabetical symbols and spaces.
text = 'World is not enough.';
//console.log(text.split('').reverse().join());
let reversedText = [];
let splittedText = text.split(' ');
let word;
for(word of splittedText){
    reversedText.push(word.split('').reverse().join(''));
}
console.log('res=' + reversedText.join(' '));
///////////////////////////////////