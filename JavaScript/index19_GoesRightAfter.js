// In a given word you need to check if one symbol goes right after another.

// Cases you should expect while solving this challenge:

//     If more than one symbol is in the list you should always count the first one
//     one of the symbols are not in the given word - your function should return False;
//     any symbol appears in a word more than once - use only the first one;
//     two symbols are the same - your function should return False;
//     the condition is case sensitive, which mease 'a' and 'A' are two different symbols.

// Input: Three arguments. The first one is a given string, second is a symbol that shoud go first, and the third is a symbold that should go after the first one.

// Output: A bool. 

function goesAfter(word, first, second) {
    let result = true;
    let firstIdx = word.search(first);
    let secondIdx = word.search(second);
    if( firstIdx == -1 || secondIdx == -1 || firstIdx == secondIdx || (firstIdx+1) != secondIdx) {
        // if both first and second appear in the word
        // and second comes right after first
        result = false;
    }
    return result;
}

console.log(goesAfter('world', 'w', 'o'));