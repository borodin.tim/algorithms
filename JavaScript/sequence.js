// infinite string 
123456789101112131415
// examples: 
// 1 = 0
// 91 = 8
// 10 = 9

const pattern = '91';

let idx = 0;
let goOn = true
let str = '1';
let regexp;
let inc;
let numIdx = 0;

while (goOn) {
    if (idx === 100) {
        goOn = false;
    }
    regexp = new RegExp(pattern, 'g');
    if (regexp.test(str)) {
        goOn = false;
    } else {
        // console.log('idx before inc: ', idx)
        inc = (idx + 2).toString().length;
        // console.log('inc:', inc);
        idx += inc;
        numIdx++;
        str += numIdx + 1;
        console.log(str)
        console.log('idx after inc: ', idx)
    }
}

console.log('result:', numIdx);//- (pattern.length - 1)