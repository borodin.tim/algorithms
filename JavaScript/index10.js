// You have a text and a list of words. You need to check if the words in a list appear in the same order as in the given text.

// Cases you should expect while solving this challenge:

//     a word from the list is not in the text - your function should return False;
//     any word can appear more than once in a text - use only the first one;
//     two words in the given list are the same - your function should return False;
//     the condition is case sensitive, which means 'hi' and 'Hi' are two different words;
//     the text includes only English letters and spaces.

// Input: Two arguments. The first one is a given text, the second is a list of words.

// Output: A bool.

let textStr = 'hi World im here';
let words = ['world'];
// let textStr = 'london in the capital of great britain",';
// let words = ['London'];
let map = {};
let res = true;
let text =  textStr.split(' ');

let indexes = [];
for (let i = 0; i < words.length; i++) {
    if(map[words[i]] == undefined){
        map[words[i]] = -1;
    } else {
        // 2 equal words found.
        console.log('2 equal words found.');
        res = false;
    }
}

for (let i = 0; i < text.length; i++) {
    if(map[text[i]] == -1 && words.includes(text[i])){
        map[text[i]] = i;
    }
}

if(true == res){

    console.log('map=' + map);
    for (var i = 0, keys = Object.keys(map), ii = keys.length; i < ii; i++) {
        console.log(keys[i] + '|' + map[keys[i]]);
    }

    for (let j = 0; j <= words.length-1; j++) {
        console.log('words[j]='+words[j]);
        console.log('map[words[j]]=' + map[words[j]]);
        console.log('map[words[j+1]]=' + map[words[j+1]]);
        if(-1 == map[words[j]] || -1 == map[words[j+1]]){
            res = false;
            break;
        }
        if (map[words[j]] > map[words[j+1]]){
            res = false;
        }
    }
}
console.log('res='+res);