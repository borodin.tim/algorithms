// Determine whether the sequence of elements items is ascending so that its 
// each element is strictly larger than (and not merely equal to) the element 
// that precedes it.
// Input: Iterable with ints.
// Output: Bool.
// The mission was taken from Python CCPS 109 Fall 2018. 
// It is taught for Ryerson Chang School of Continuing Education by Ilkka Kokkarinen

let values = [-5, 10, 99, 123456];
let res = true;

for (let i = 0; i < values.length-1; i++) {
    if(values[i] >= values[i+1]){
        res = false;
        break
    }
}
console.log('res='+res);