let absoluteMin = 10000;

// var inputs = '-273';
// var inputs = '-15 -7 -9 -14 -12';
// var inputs = '-10 -10';
var inputs = '15 -7 9 14 7 12';

if (inputs && inputs.length > 0) {
    const arr = inputs.split(' ');
    for (let i = 0; i < arr.length; i++) {
        const t = parseInt(arr[i]);
        if (!isNaN(t) && Math.abs(absoluteMin) >= Math.abs(t)) {
            if (Math.abs(absoluteMin) === Math.abs(t)) {
                absoluteMin = Math.abs(t);
            } else {
                absoluteMin = t;
            }
        }
    }
}

if (absoluteMin === 10000) {
    absoluteMin = 0;
}
console.log(absoluteMin);
