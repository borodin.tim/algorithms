const n = parseInt('1');
const line = 'b';
var inputs = '2';
let result = '';

if (!isNaN(n) && line && line.length > 0 && inputs && inputs.length > 0) {
    const inputsArr = inputs.split(' ');
    for (let i = 0; i < n; i++) {
        const index = parseInt(inputsArr[i]);
        if (index <= line.length) {
            result += line.charAt(index - 1);
        }
    }
}

console.log(result);