const n = '101';
let sum = 0;
let guestsAmount = 0;

if (n && n.length > 0) {
    guestsAmount = parseInt(n);
    if (!isNaN(guestsAmount) && guestsAmount > 0) {
        for (let i = 1; i <= guestsAmount - 1; i++) {
            sum += i
        }
    }
}

console.log(sum);
