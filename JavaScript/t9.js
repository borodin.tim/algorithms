const width = parseInt(readline());
const arr = [
    [' ', ' ', '', '', ''],
    [' ', ' ', '', '', ''],
    ['H', '-', 'C', '=', 'C'],
    [' ', ' ', '', '', ''],
    [' ', ' ', '', '', ''],
];
for (let i = 0; i < 5; i++) {
    const row = readline();
    arr.push(row);
}
console.error(arr)

bondsCount = 0;
doubleBondsCount = 0;
for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < arr[i].length; j++) {
        if (arr[i][j] === 'C' && (arr[i - 1][j] === '|')) {
            bondsCount++;
        }
        if (arr[i][j] === 'C' && (arr[i - 1][j] === '||')) {
            bondsCount += 2;
            doubleBondsCount++;
        }

        if (arr[i][j] === 'C' && (arr[i][j + 1] === '-')) {
            bondsCount++;
        }
        if (arr[i][j] === 'C' && (arr[i][j + 1] === '=')) {
            bondsCount += 2;
            doubleBondsCount++;
        }

        if (arr[i][j] === 'C' && (arr[i + 1][j] === '|')) {
            bondsCount++;
        }
        if (arr[i][j] === 'C' && (arr[i + 1][j] === '||')) {
            bondsCount += 2;
            doubleBondsCount++;
        }

        if (arr[i][j] === 'C' && (arr[i][j - 1] === '-')) {
            bondsCount++;
        }
        if (arr[i][j] === 'C' && (arr[i][j - 1] === '=')) {
            bondsCount += 2;
            doubleBondsCount++;
        }
    }
}

console.error('bondsCount:', bondsCount)
console.error('doubleBondsCount:', doubleBondsCount)

if (bondsCount === 4) {
    if (doubleBondsCount === 0) {
        console.log('ALKANE');
    } else if (doubleBondsCount === 1) {
        console.log('ALKENE');
    } else if (doubleBondsCount >= 2) {
        console.log('OTHER');
    } else {
        console.log('ERROR');
    }
}


