const btn5 = document.getElementById('btn5');

btn5.addEventListener('click', () => {
    const arrBtns = [];
    const arrBtns2 = [];
    
    for (let i = 0; i < 9; i++) {
        arrBtns.push(document.getElementById('btn' + (i + 1)));    
        arrBtns2.push(document.getElementById('btn' + (i + 1)).innerHTML);
    }

    arrBtns[0].innerHTML = arrBtns2[3];
    arrBtns[1].innerHTML = arrBtns2[0];
    arrBtns[2].innerHTML = arrBtns2[1];

    arrBtns[3].innerHTML = arrBtns2[6];
    arrBtns[5].innerHTML = arrBtns2[2];

    arrBtns[6].innerHTML = arrBtns2[7];
    arrBtns[7].innerHTML = arrBtns2[8];
    arrBtns[8].innerHTML = arrBtns2[5];
});