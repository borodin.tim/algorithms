let n1;
let n2;
let op;

const res = document.getElementById('res');

const btn0 = document.getElementById('btn0');
const btn1 = document.getElementById('btn1');
const btnClr = document.getElementById('btnClr');
const btnEql = document.getElementById('btnEql');
const btnSum = document.getElementById('btnSum');
const btnSub = document.getElementById('btnSub');
const btnMul = document.getElementById('btnMul');
const btnDiv = document.getElementById('btnDiv');

btn0.addEventListener('click', () => {
    res.value += 0;
});
btn1.addEventListener('click', () => {
    res.value += 1;
});
btnClr.addEventListener('click', () => {
    res.value = '';
});
btnEql.addEventListener('click', () => {
    n2 = res.value.split(op)[1];

    switch (op) {
        case '+': res.value = Number(parseInt(n1, 2) + parseInt(n2, 2)).toString(2);
            break;
        case '-': res.value = Number(parseInt(n1, 2) - parseInt(n2, 2)).toString(2);
            break;
        case '*': res.value = Number(parseInt(n1, 2) * parseInt(n2, 2)).toString(2);
            break;
        case '/': res.value = Math.floor(Number(parseInt(n1, 2) / parseInt(n2, 2)).toString(2));
            break;
    }

});
btnSum.addEventListener('click', () => {
    n1 = res.value;
    op = '+';
    res.value += '+';
});
btnSub.addEventListener('click', () => {
    n1 = res.value;
    op = '-';
    res.value += '-';
});
btnMul.addEventListener('click', () => {
    n1 = res.value;
    op = '*';
    res.value += '*';
});
btnDiv.addEventListener('click', () => {
    n1 = res.value;
    op = '/';
    res.value += '/';
});

